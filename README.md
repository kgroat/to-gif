# to-gif
`to-gif` is a simple utility for converting movies to gifs.  It utilizes the power of `ffmpeg` to quickly crop, resize, and convert.

## Installation
```bash
npm i @thegrtman/to-gif
```

## CLI
To quickly convert a movie, you can run:
```bash
to-gif my-movie.mp4
```

To specify an output file at the time you issue the command, you can pass it as a second argument:
```bash
to-gif path/to/input.mp4 path/to/output.gif
```

The CLI will automatically ask questions regarding what portion of the movie should be used, how it should be cropped, and how it should be resized.

## Node API
To use the node api, you can:
```js
const { toGif } = require('@thegrtman/to-gif');

exports.convertFile = function convertFile () {
  return toGif('./path/to/input.mp4', './path/to/output.gif').then(() => {
    console.log('Done!');
  });
}
```

By default, the entire movie will be converted to a gif at 15 frames per second.

If instead you want to specify what portion of the movie, how to crop it, etc., you can:
```js
const { toGif } = require('@thegrtman/to-gif');

exports.convertFile = function convertFile () {
  return toGif('./path/to/input.mp4', './path/to/output.gif', {
    answers: {
      startTime: 362.5, // start time, in seconds
      duration: 5, // length of the gif, in seconds
      framerate: 20, // use 20 frames per second
      resizeWidth: 200, // output a gif 200 pixels wide
      resizeHeight: 100, // and 100 pixels tall
      cropTop: 5, // crop 5 pixels from the top
      cropBottom: 50, // crop 50 pixels from the bottom
      cropLeft: 10, // crop 10 pixels from the left
      cropRight: 10, // crop 10 pixels from the right
    },
  }).then(() => {
    console.log('Done!');
  });
}
```

If you instead want the API to ask for answers over standard input, you can:
```js
const { toGif } = require('@thegrtman/to-gif');

exports.convertFile = function convertFile () {
  return toGif('./path/to/input.mp4', './path/to/output.gif', {
    askForAnswers: true,
  }).then(() => {
    console.log('Done!');
  });
}
```

You can also provide defaults, but still ask for answers:
```js
const { toGif } = require('@thegrtman/to-gif');

exports.convertFile = function convertFile () {
  return toGif('./path/to/input.mp4', './path/to/output.gif', {
    answers: {
      duration: 5,
      framerate: 30,
    },
    askForAnswers: true,
  }).then(() => {
    console.log('Done!');
  });
}
```

## Typescript
The library also provides typescript definitions files, so you can use it in your own ts-node project:
```ts
import { toGif } from '@thegrtman/to-gif';

export async function convertFile() {
  await toGif('./path/to/input.mp4', './path/to/output.gif');
  console.log('Done!');
}
```
