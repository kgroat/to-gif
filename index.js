#!/usr/bin/env node

process.env.TO_GIF_LOG_LEVEL = 'DEBUG'

require('ts-node/register')
require('./src/index.ts')
