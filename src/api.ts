
import { access, constants } from 'fs'
import { promisify } from 'util'
import { getInfo } from './getInfo'
import { printInfo } from './printInfo'
import { getAnswers, getDefaultAnswers, Answers } from './getAnswers'
import { convertToGif } from './convertToGif'
import { NoFileSpecifiedError, AccessError } from './errors'
import { LogLevel, buildLogger } from './logging'
const accessAsync = promisify(access)

export { Answers } from './getAnswers'
export * from './errors'

export interface ToGifOptions {
  askForAnswers?: boolean
  answers?: Answers | null
  logLevel?: LogLevel | null
}

type FullToGifOptions = Required<ToGifOptions>

const DEFAULT_OPTIONS: FullToGifOptions = {
  askForAnswers: false,
  answers: null,
  logLevel: null,
}

export async function toGif (inputFile: string, outputFile?: string, opts?: ToGifOptions) {
  if (!inputFile) {
    throw new NoFileSpecifiedError()
  }

  try {
    await accessAsync(inputFile, constants.R_OK)
  } catch (e) {
    const { code } = e as NodeJS.ErrnoException
    throw new AccessError(code || 'EACCESS', inputFile)
  }

  const fullOpts: FullToGifOptions = {
    ...DEFAULT_OPTIONS,
    ...opts,
  }

  const {
    logLevel,
    askForAnswers,
    answers: defaultAnswers,
  } = fullOpts

  const logger = buildLogger(logLevel)
  const info = await getInfo(inputFile)
  printInfo(info, logger)

  const answers = askForAnswers
    ? await getAnswers(inputFile, info, outputFile, defaultAnswers)
    : getDefaultAnswers(inputFile, info, outputFile, defaultAnswers)

  logger.debug('Answers', JSON.stringify(answers, null, 2))

  await convertToGif(logger, inputFile, info, answers)
}
