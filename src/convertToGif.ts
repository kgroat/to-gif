
import { spawn } from 'child_process'
import { AllAnswers, CropMode, ResizeMode } from './getAnswers'
import { MovieInfo, Dimensions } from './getInfo'
import { ConversionFailedError } from './errors'
import { Logger } from './logging'

function spawnAsync (command: string, args: string[]) {
  return new Promise<void>((resolve, reject) => {
    const child = spawn(command, args, { stdio: 'inherit' })
    child.on('exit', (code, signal) => {
      if (code !== 0 || signal) {
        reject()
      } else {
        resolve()
      }
    })
  })
}

function getCropFormat ({ width, height }: Dimensions, answers: AllAnswers) {
  const {
    cropTop,
    cropBottom,
    cropLeft,
    cropRight,
  } = answers
  const newHeight = height - cropTop - cropBottom
  const newWidth = width - cropLeft - cropRight
  return `${newWidth}:${newHeight}:${cropLeft}:${cropTop}`
}

function getResizeFormat (answers: AllAnswers) {
  return `${answers.resizeWidth}:${answers.resizeHeight}`
}

// ffmpeg -i tree-scroll.mov -vf "fps=15,crop=289:418:0:0,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -f gif temp2.gif
export async function convertToGif (logger: Logger, inputFile: string, info: MovieInfo, answers: AllAnswers): Promise<void> {
  const {
    startTime,
    duration,
    cropMode,
    resizeMode,
    filename,
  } = answers

  const args = [
    '-i', inputFile,
    '-y',
  ]
  let format = 'fps=15,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse'

  if (cropMode !== CropMode.NO_CROP) {
    format = `crop=${getCropFormat(info.dimensions!, answers)},${format}`
  }

  if (resizeMode !== ResizeMode.NO_RESIZE) {
    format = `scale=${getResizeFormat(answers)},${format}`
  }

  args.push('-vf', format)

  if (startTime > 0) {
    args.push('-ss', startTime.toString(10))
  }

  args.push('-t', duration.toString(10))
  args.push('-f', 'gif', filename)

  logger.debug('ffmpeg', args.map(a => a.startsWith('-') ? `\n  ${a}` : `"${a}"`).join(' '))
  try {
    await spawnAsync('ffmpeg', args)
  } catch {
    throw new ConversionFailedError(inputFile)
  }
}
