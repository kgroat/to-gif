
export enum ErrorCode {
  UNKNOWN_ERROR = 1,
  NO_FILE_SPECIFIED = 2,
  CANNOT_READ_FILE = 3,
  FILE_NOT_MOVIE = 4,
  CONVERSION_FAILED = 5,
}

export abstract class CodedError extends Error {
  public readonly code: ErrorCode

  constructor (message: string, code: ErrorCode) {
    super(message)
    this.code = code
  }
}

export class NoFileSpecifiedError extends CodedError {
  constructor () {
    super('Please supply an input file.', ErrorCode.NO_FILE_SPECIFIED)
  }
}

export class AccessError extends CodedError {
  constructor (errCode: string, filename: string) {
    super(`${errCode} -- Cannot read ${filename}`, ErrorCode.CANNOT_READ_FILE)
  }
}

export class NotMovieError extends CodedError {
  constructor (inputFile: string) {
    super(`${inputFile} is not a movie.`, ErrorCode.FILE_NOT_MOVIE)
  }
}

export class ConversionFailedError extends CodedError {
  constructor (inputFile: string) {
    super(`Failed to convert ${inputFile}`, ErrorCode.CONVERSION_FAILED)
  }
}
