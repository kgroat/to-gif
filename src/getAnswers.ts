
import enquirer from 'enquirer'
import { MovieInfo, Duration, Dimensions } from './getInfo'

export enum CropMode {
  NO_CROP = 'don\'t crop',
  CROP_BOTTOM = 'crop from the bottom',
  CROP_TOP = 'crop from the top',
  CROP_LEFT = 'crop from the left',
  CROP_RIGHT = 'crop from the right',
  FULL_CROP = 'custom crop',
}

export const singleCropModes = [
  CropMode.CROP_BOTTOM,
  CropMode.CROP_RIGHT,
  CropMode.CROP_TOP,
  CropMode.CROP_LEFT,
]

export const cropModes = [
  CropMode.NO_CROP,
  ...singleCropModes,
  CropMode.FULL_CROP,
]

export enum ResizeMode {
  NO_RESIZE = 'don\'t resize',
  SPECIFY_HEIGHT = 'specify height',
  SPECIFY_WIDTH = 'specify width',
  SPECIFY_BOTH = 'specify width and height',
}

export const resizeModes = [
  ResizeMode.NO_RESIZE,
  ResizeMode.SPECIFY_WIDTH,
  ResizeMode.SPECIFY_HEIGHT,
  ResizeMode.SPECIFY_BOTH,
]

export interface Answers {
  /**
   * The number of seconds to skip from the start of the movie before the gif should start
   */
  startTime?: number

  /**
   * How long the gif should be, in seconds
   */
  duration?: number

  /**
   * How many pixels to crop from the top of the gif
   */
  cropTop?: number

  /**
   * How many pixels to crop from the bottom of the gif
   */
  cropBottom?: number

  /**
   * How many pixels to crop from the left of the gif
   */
  cropLeft?: number

  /**
   * How many pixels to crop from the right of the gif
   */
  cropRight?: number

  /**
   * How wide the output gif should be; if not specified, will be determined automatically
   */
  resizeWidth?: number

  /**
   * How tall the output gif should be; if not specified, will be determined automatically
   */
  resizeHeight?: number

  /**
   * The frames per second of the output gif
   */
  framerate?: number
}

export interface AllAnswers extends Required<Answers> {
  /**
   * The name of the output file
   */
  filename: string

  /**
   * Whether, and how, the movie should be cropped
   */
  cropMode: CropMode

  /**
   * Whether, and how, the movie should be resized
   */
  resizeMode: ResizeMode
}

function convertDurationToSeconds (duration: Duration) {
  const { hours, minutes, seconds } = duration
  return hours * 3600 + minutes * 60 + seconds
}

const validateMax = (max: number) => (value: string) => {
  const num = parseFloat(value)
  if (num > max) {
    return `Should be below ${max}`
  }
  return true
}

export function getDefaultAnswers (inputFile: string, info: MovieInfo, outputFile?: string, defaultAnswers?: Answers | null): AllAnswers {
  const { duration } = info
  return {
    filename: outputFile || inputFile.replace(/\.\w+$/, ''),
    startTime: 0,
    duration: duration ? convertDurationToSeconds(duration) : 10,
    framerate: 15,
    resizeMode: ResizeMode.SPECIFY_BOTH,
    resizeWidth: -2,
    resizeHeight: -2,
    cropMode: CropMode.FULL_CROP,
    cropTop: 0,
    cropBottom: 0,
    cropLeft: 0,
    cropRight: 0,
    ...(defaultAnswers || {}),
  }
}

async function getResizeAnswers (answers: AllAnswers, { width, height }: Dimensions): Promise<AllAnswers> {
  answers = {
    ...answers,
    ...await enquirer.prompt<AllAnswers>({
      name: 'resizeMode',
      message: `Do you want to resize the video?`,
      type: 'select',
      choices: resizeModes,
    }),
  }

  if (answers.resizeMode === ResizeMode.SPECIFY_WIDTH || answers.resizeMode === ResizeMode.SPECIFY_BOTH) {
    answers = {
      ...answers,
      ...await enquirer.prompt<AllAnswers>({
        name: 'resizeWidth',
        message: `What width do you want to resize to?`,
        type: 'numeral',
        float: false,
        min: 0,
        initial: width,
      }),
    }
  }

  if (answers.resizeMode === ResizeMode.SPECIFY_HEIGHT || answers.resizeMode === ResizeMode.SPECIFY_BOTH) {
    answers = {
      ...answers,
      ...await enquirer.prompt<AllAnswers>({
        name: 'resizeHeight',
        message: `What height do you want to resize to?`,
        type: 'numeral',
        float: false,
        min: 0,
        initial: height,
      }),
    }
  }

  return answers
}

async function getCropAnswers (answers: AllAnswers, dimensions: Dimensions): Promise<AllAnswers> {
  const { width, height } = dimensions
  let currentWidth = answers.resizeWidth > 0 ? answers.resizeWidth : width
  let currentHeight = answers.resizeHeight > 0 ? answers.resizeHeight : height
  answers = {
    ...answers,
    ...await enquirer.prompt<AllAnswers>({
      name: 'cropMode',
      message: `Do you want to crop the video?`,
      type: 'select',
      choices: cropModes,
    }),
  }

  if (CropMode.NO_CROP === answers.cropMode) {
    return answers
  }

  if (currentHeight > 1 && answers.cropMode === CropMode.CROP_TOP || answers.cropMode === CropMode.FULL_CROP) {
    answers = {
      ...answers,
      ...await enquirer.prompt<AllAnswers>({
        name: 'cropTop',
        message: `How much do you want to crop from the TOP?`,
        type: 'numeral',
        float: false,
        min: 0,
        initial: 0,
        max: currentHeight - 1,
        validate: validateMax(currentHeight - 1),
      }),
    }
    currentHeight -= answers.cropTop
  }

  if (currentHeight > 1 && answers.cropMode === CropMode.CROP_BOTTOM || answers.cropMode === CropMode.FULL_CROP) {
    answers = {
      ...answers,
      ...await enquirer.prompt<AllAnswers>({
        name: 'cropBottom',
        message: `How much do you want to crop from the BOTTOM?`,
        type: 'numeral',
        float: false,
        min: 0,
        initial: 0,
        max: currentHeight - 1,
        validate: validateMax(currentHeight - 1),
      }),
    }
    currentHeight -= answers.cropBottom
  }

  if (currentWidth > 1 && answers.cropMode === CropMode.CROP_LEFT || answers.cropMode === CropMode.FULL_CROP) {
    answers = {
      ...answers,
      ...await enquirer.prompt<AllAnswers>({
        name: 'cropLeft',
        message: `How much do you want to crop from the LEFT?`,
        type: 'numeral',
        float: false,
        min: 0,
        initial: 0,
        max: currentWidth - 1,
        validate: validateMax(currentWidth - 1),
      }),
    }
    currentWidth -= answers.cropLeft
  }

  if (currentWidth > 1 && answers.cropMode === CropMode.CROP_RIGHT || answers.cropMode === CropMode.FULL_CROP) {
    answers = {
      ...answers,
      ...await enquirer.prompt<AllAnswers>({
        name: 'cropRight',
        message: `How much do you want to crop from the RIGHT?`,
        type: 'numeral',
        float: false,
        min: 0,
        initial: 0,
        max: currentWidth - 1,
        validate: validateMax(currentWidth - 1),
      }),
    }
    currentWidth -= answers.cropRight
  }

  return answers
}

export async function getAnswers (inputFile: string, info: MovieInfo, outputFile?: string, defaultAnswers?: Partial<AllAnswers> | null) {
  let answers = getDefaultAnswers(inputFile, info, outputFile, defaultAnswers)
  const { duration, dimensions } = info

  if (!outputFile) {
    answers = {
      ...answers,
      ...await enquirer.prompt<AllAnswers>({
        name: 'filename',
        message: `What should the filename be?`,
        type: 'input',
        initial: answers.filename,
        format: (name) => `${name}.gif`,
        result: (name) => `${name}.gif`,
      }),
    }
  }

  if (duration) {
    const durationInSeconds = convertDurationToSeconds(duration)

    answers = {
      ...answers,
      ...await enquirer.prompt<AllAnswers>({
        name: 'startTime',
        message: `When should the gif start from? (max ${durationInSeconds})`,
        type: 'numeral',
        float: true,
        initial: 0,
        min: 0,
        max: durationInSeconds,
        validate: validateMax(durationInSeconds),
      }),
    }

    const remainingDuration = durationInSeconds - answers.startTime
    if (remainingDuration < 1) {
      answers.duration = remainingDuration
    } else {
      answers = {
        ...answers,
        ...await enquirer.prompt<AllAnswers>({
          name: 'duration',
          message: `How long should the gif be? (max ${remainingDuration})`,
          type: 'numeral',
          float: true,
          initial: Math.min(10, remainingDuration),
          min: 0.05,
          max: remainingDuration,
          validate: validateMax(remainingDuration),
        }),
      }
    }
  }

  if (dimensions) {
    answers = {
      ...answers,
      ...await getResizeAnswers(answers, dimensions),
    }
    answers = {
      ...answers,
      ...await getCropAnswers(answers, dimensions),
    }
  }

  answers = {
    ...answers,
    ...await enquirer.prompt<AllAnswers>({
      name: 'framerate',
      message: `What framerate do you want to use?`,
      type: 'numeral',
      float: false,
      initial: answers.framerate,
      min: 1,
      max: 60,
    }),
  }

  return answers
}
