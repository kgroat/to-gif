
import { exec } from 'child_process'
import { promisify } from 'util'
import { NotMovieError } from './errors'

const execAsync = promisify(exec)

export interface Duration {
  hours: number
  minutes: number
  seconds: number
}

export interface Dimensions {
  width: number
  height: number
}

export interface MovieInfo {
  duration: Duration | null
  dimensions: Dimensions | null
}

const VIDEO_FINDER = /Input.+(mov|mp4|m4a|3gp|3g2|mj2)/m
const DURATION_FINDER = /Duration: ([0-9:\.]+),/m
const DIMENSIONS_FINDER = /Stream .+ Video:(?:(?:[^,\(\)]+(?:\([^\)]+\)))*,)*\s+(\d+)x(\d+)/m
export async function getInfo (filename: string): Promise<MovieInfo> {
  let stdout: string
  try {
    const result = await execAsync(`ffmpeg -i ${filename} 2>&1`)
    stdout = result.stdout
  } catch (e) {
    stdout = e.stdout
  }

  const isVideo = VIDEO_FINDER.test(stdout)
  if (!isVideo) {
    throw new NotMovieError(filename)
  }

  let duration: Duration | null = null
  let dimensions: Dimensions | null = null
  if (DURATION_FINDER.test(stdout)) {
    const [, durationString] = DURATION_FINDER.exec(stdout)!
    let [hours, minutes, seconds] = durationString.split(':').map(parseFloat)
    if (isNaN(seconds)) {
      seconds = minutes
      minutes = hours
      hours = 0
    }
    if (isNaN(seconds)) {
      seconds = minutes
      minutes = 0
    }
    duration = {
      hours,
      minutes,
      seconds,
    }
  }

  if (DIMENSIONS_FINDER.test(stdout)) {
    const [, width, height] = DIMENSIONS_FINDER.exec(stdout)!.map(n => parseInt(n, 10))
    dimensions = {
      width,
      height,
    }
  }

  return {
    duration,
    dimensions,
  }
}
