#!/usr/bin/env node
import colors from 'ansi-colors'
import { toGif } from './api'

async function main () {
  await toGif(process.argv[2], process.argv[3], { askForAnswers: true })
  console.log('Done!')
}

main().catch(e => {
  if (e instanceof Error) {
    console.error(colors.bold.red(e.message))
  } else {
    console.error(colors.bold.red(e))
  }

  if (e.code) {
    process.exit(e.code)
  } else {
    process.exit(1)
  }
})
