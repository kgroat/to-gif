
export enum LogLevel {
  NONE = 1,
  FATAL = 2,
  ERROR = 3,
  WARN = 4,
  INFO = 5,
  DEBUG = 6,
}

export type LogLevelName = 'NONE' | 'FATAL' | 'ERROR' | 'WARN' | 'INFO' | 'DEBUG'

export type Logger = ReturnType<typeof buildLogger>

export function getDefaultLevel () {
  let level = process.env.TO_GIF_LOG_LEVEL!
  if (level) { level = level.toUpperCase() }
  return LogLevel[level as LogLevelName] || LogLevel.WARN
}

export function getCurrentLevel (logLevel?: LogLevel | null) {
  return logLevel || getDefaultLevel()
}

const noop = () => undefined
export function buildLogger (currentLevel?: LogLevel | null) {
  currentLevel = getCurrentLevel(currentLevel)
  return {
    fatal: currentLevel <= LogLevel.FATAL ? console.error : noop,
    error: currentLevel <= LogLevel.ERROR ? console.error : noop,
    warn: currentLevel <= LogLevel.WARN ? console.warn : noop,
    info: currentLevel <= LogLevel.INFO ? console.log : noop,
    debug: currentLevel <= LogLevel.DEBUG ? console.debug : noop,
  }
}
