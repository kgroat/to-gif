
import { MovieInfo } from './getInfo'
import { Logger } from './logging'

export function printInfo ({ duration, dimensions }: MovieInfo, logger: Logger) {
  if (duration) {
    const { hours, minutes, seconds } = duration
    if (hours) {
      logger.info(`Duration: ${hours}:${minutes}:${seconds}`)
    } else if (minutes) {
      logger.info(`Duration: ${minutes}:${seconds}`)
    } else {
      logger.info(`Duration: ${seconds} seconds`)
    }
  }

  if (dimensions) {
    const { width, height } = dimensions
    logger.info(`Dimensions: ${width}x${height}`)
  }
}
